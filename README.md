# GS2 Dockerfiles

This repository contains Dockerfiles for the images used to run GS2's testing
pipelines.

The repository is linked to the Dockerhub repository 
[josephparker/gs2-dockerfiles](https://hub.docker.com/r/josephparker/gs2-dockerfiles/builds/)
which hosts the images used in pipelines.

## Using images

Using images saves time in a pipeline, as it is not necessary to set-up the
build environment and build dependencies.

To use an existing image in a pipeline, set 
```
        image:
          name: josephparker/gs2-dockerfiles:<image_name>
          run-as-user: 1000
```
in `bitbucket-pipelines.yml`. Here `<image_name>` is the image's Dockerhub
tag. The `run-as-user` option is necessary to prevent errors from trying to run
`mpirun` as root. The pipeline will now load this image and run the commands
included in the script argument, for example
```
        script:
          - bash <test_script_path>/<test_script_name>.sh
```
Test scripts are versioned in the main GS2 repository in the folder 
`pipeline_test_scripts`. The name of test scripts should match the Docker 
image tag.

## Creating images

To set up a new build environment, clone this repository:
```
git clone git@git.gyrokinetics.gs2-dockerfiles.git
```
and make a new directory with the same name as your new image. In this
directory make a file called Dockerfile.  It is easiest to base this on
existing Dockerfiles in the repository. In this file, include commands to set
up your build environment as if you were calling them at command line on clean
install of the operating system.

### Building images

Images may be tested by building locally. To do this, run
```
sudo docker build -t josephparker/gs2-dockerfiles:<image_name> -f Dockerfile .
```
in the same directory as your `Dockerfile`. Note that this must be run as root.
Also note the trailing "."!

This operation will end with the lines:
```
Successfully built <image_hash>
Successfully tagged josephparker/gs2-dockerfiles:<image_name>
```

### Running locally

To run the image locally, do
```
sudo docker run -i -t <image_hash>
```
This will open a command line that will allow you to test your image. For 
example, you can test if the commands required by the test script work in this
environment.

### Pushing the Dockerfile

Once you are happy with the image, commit the Dockerfile to the repository:
```
git add <image_name>/Dockerfile
git commit -m "My detailed commit message"
git push
```

### Remote build

To build an image on Dockerhub, go to 
[build settings](https://hub.docker.com/r/josephparker/gs2-dockerfiles/~/settings/automated-builds/)
and press the button "trigger" on the line corresponding to your image. 
Progress of builds may be seen in the 
[build details](https://hub.docker.com/r/josephparker/gs2-dockerfiles/builds/) 
section.

If you need to build a new image, click the green plus sign. File in the new
line with:  

|                     |                |
|:--------------------|:---------------|
| Type                | tag            |
| Name                | <image_name>   |
| Dockerfile Location | /<image_name>  |
| Docker Tag Name     | <image_name>   |

Note the leading slash in Dockerfile Location - it's the location relative to
the top directory of the repo.

#### Automatic build

The new image may now be build automatically by pushing its Dockerfile with the
tag <image_name>.  That is:
```
git add <image_name>/Dockerfile
git commit -m "Message"
git tag <image_name>
git push --tags
```
